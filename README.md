# Tool Name: Ultra Generator
## By: Dennis Taylor (Alias: Denchyaknow)

## Features: 
- Easily generate height maps using Cellular Automata styled algorithms
- Adjust the rules of the Cellular Growth in the algorithms
- Use controls tied to varied algorithms to adjust the floors, walls, and both per iteration of growth
- Save and load Height maps along with the name, resolution, and width/height
- Give Height maps an adjustable padded border as floors or walls
- Set Height maps to terrain with adjustable Height map resolution
- Set Terrain Width Depth and Height and have the generated Height map scale accordingly

## Usage:
- Import package into Unity (Tested and Created with Unity 2018.1.4)
- Open Editor window via Menu item Tools/UltraGenerator
- Randomly Generate map and smooth to liking.

### Time span: (June 23, 2018) - (June 26, 2018)

### June 23, 2018

- Wrote rough draft of map framework and algorithms using a 3 dimensional array.
- Wrote algorithms to populate the 3D array with blocks and randomly generate placing in a cubic environment
- Added some GUI controls to Iterate algorithms
- Wrote quick Gizmos method to display map as generated

I was being quite ambitious when I decided to use a 3 dimensional array, I learned that the rendering time was slow and did not scale well when the cellular growth algorithms adjusted the blocks.


#### Plotting points randomly would give me 
[pic0]: https://drive.google.com/uc?export=download&id=1gStc-WlWGg4_khhMkv0Sw103o3HvGa1g
![alt text][pic0]


#### Though i found the results promising using a 3D array to hold data points scalability would need to take a lot more prototyping to be used with a user friendly interface and down to pixel scale.
[pic1]: https://drive.google.com/uc?export=download&id=11rzMvClyBfO5AmPEiC9I3D5V6QDJFxCz
![alt text][pic1]

#### Example of a 80x10x80 generation
[pic2]: https://drive.google.com/uc?export=download&id=1K6092Hd96H71TiYLv5VgUW6vPpJi1ocl
![alt text][pic2]

### June 24, 2018

- Added more Growth algorithms
- Added algorithms to adjust texture of height map for export and saving
- Added height map importing and drawing algorithms

I spent most of the day trying to export the 3D array of data points to a height map to be used with a terrain. I tried a lot of different algorithms to simplify the user controls in the editor.
Eventually I realized the controls would need a lot of adjustable variables unless I took out a lot of the flexibility of using a 3D array. I also learned that a terrain height map would not be able to fully utilize all of the features a 3D array would provide such as angled walls, ceilings, tunnels, etc.

#### Growth Algorithms
[pic3]: https://drive.google.com/uc?export=download&id=1hwZFrJ_jc_tChLaVBxy51GbdrFw8SrW6
![alt text][pic3]

Applying the 3D array to a Terrain was not worth the excess calulation at this time.
[pic4]: https://drive.google.com/uc?export=download&id=1I0idLT9QBeIXwXolijDS5Ob6mYfZ_uyQ
![alt text][pic4]


### June 25, 2018

- Rewrote Framework from scratch under the namespace: Ultra Generator
- Changed Blocks class to Cell struct to speed of processing
- Rewrote editor controls and GUI from scratch for final draft
- Ditched the 3D array for a 2D array

I decided to rewrite the framework from scratch as the scripts that contained thousands of commented and pseudo code would take longer to clean up and convert from using a 3D array to a 2D array. Rewriting the algorithms would be quicker and there would be less controls than the previous framework.

The 2D array proved to be much quicker when generating the relative cells for data points of the Height map. Setting the Height map to the terrain data showed promise.

#### Example Generated Terrain
[pic5]: https://drive.google.com/uc?export=download&id=1G3Dlc5kvK2sF_SIo2Gx_86YtCxAduzow
![alt text][pic5]
[pic6]: https://drive.google.com/uc?export=download&id=13EbcFjFeHX6EBue90rXqryfQWaXd1wyQ
![alt text][pic6]


### June 26, 2018

- Added algorithms for border padding and options
- Added controls and methods for changing resolution of Height map
- Added exporting and saving of height maps
- Added tool tips and GUI styling

I spent the day finalizing features and user controls to be easily used as possible. With more time I would’ve liked to add texture generation to match the walls and floors of the terrain. 

#### Example of Final GUI controls in custom editor window
[pic7]: https://drive.google.com/uc?export=download&id=1gs6FxH0QsMNzWWAcDriEWSTxPJaxZ4jV
![alt text][pic7]

#### Example of Border padding set as walls
[pic8]: https://drive.google.com/uc?export=download&id=1jxrvK6JrqvdagKy5EscALHGOS3021WKJ
![alt text][pic8]

#### Example Growth Iteration
[pic9]: https://drive.google.com/uc?export=download&id=1OtwZ0ctnHAV2mmpn79opGudeyVj0yjzy
![alt text][pic9]


#### Final Notes

I enjoyed writing this plugin and found it entertaining coming up with the growth algorithms used in the 3D and 2D arrays. I plan on continuing developing this tool for usage with my personal projects and hope to eventually convert it to full custom mesh generation over time.
I plan to add Texture mapping and Object spawning for lighting effects, foliage and dynamic weather biomes, alongside infinite pooling and expanding world generation. As long as it would take, I find the challenge exhilarating!


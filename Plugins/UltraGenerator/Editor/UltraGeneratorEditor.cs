﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Denchyaknow.UltraGenerator;

[CustomEditor(typeof(UltraGenerator))]
public class UltraGeneratorEditor : Editor
{
	public override void OnInspectorGUI()
	{
		if (GUILayout.Button("Open in Ultra Generator Window"))
		{
			UltraGeneratorWindow.ShowWindow();
		}
	}
	
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Denchyaknow.UltraGenerator;
public class UltraGeneratorWindow : EditorWindow
{
	private static UltraGeneratorWindow _winInstance;
	private static UltraGenerator _mapInstance;
	private static Terrain _terrainInstance;
	private static Texture2D _mapTexture;
	private static Texture2D _mapTextureLoad;
	private static List<Texture2D> _loadContent;
	private static string _saveName;
	private static int _loadID;
	private static float _loadSize;
	private static Vector2 _winScroll;
	private static Vector2 _loadScroll;
	private static Color color0 = new Color(1f,0.8f,1f,1f);
	private static Color color1 = new Color(0.8f, 1f, 0.8f,1f);
	private static Color color2 = new Color(0.7f, 1f, 0.9f,1f);
	private static Color color3 = new Color(1f, 0.8f, 0.8f,1f);
	private static Color color4 = new Color(0.8f, 0.8f, 1f,1f);
	private static Color color5 = new Color(0.5f, 0.5f, 0.5f,1f);
	private static Color color6 = new Color(1f, 0.9f, 0.8f,1f);
	private const string savePath = "Maps";
	private const string pluginPath = "Assets/Plugins/UltraGenerator";

	[MenuItem("Tools/Ultra Map Generator")]
	public static void ShowWindow()
	{
		_winInstance = GetWindow<UltraGeneratorWindow>(true, "Ultra Generator");
		_loadSize = 100;
		if (!SingletonIsValid())
			return;
	}

	private void OnGUI()
	{
		if (!SavePathIsValid())
			return;


		if (!SingletonIsValid())
			return;

		if (_mapInstance.map == null)
		{
			_mapInstance.map = new Map(_mapInstance.mapPadding, _mapInstance.activePadding);
			_mapInstance.Initialize();

			_mapInstance.map.SmoothAll(_mapInstance.genSmoothLevel, _mapInstance.genSmoothRange, _mapInstance.mapPadding);
			RefreshHeightMap();
			SetHeightMap();
		}
		float normalLabelWidth = EditorGUIUtility.labelWidth;
		EditorGUIUtility.labelWidth = EditorGUIUtility.currentViewWidth / 4;

		_winScroll = EditorGUILayout.BeginScrollView(_winScroll);
		Color oColor = GUI.backgroundColor;

		GUI.backgroundColor = color5;
		using (var scope = new GUILayout.HorizontalScope(EditorStyles.helpBox, GUILayout.Width(EditorGUIUtility.currentViewWidth * 0.95f)))
		{
			GUIContent mapContent = new GUIContent((_mapTexture));
			using (var Scope = new GUILayout.HorizontalScope(EditorStyles.helpBox))
			{
				GUILayout.Label(mapContent);
				GUILayout.FlexibleSpace();
				GUI.backgroundColor = color6;
				using (var SubScope = new GUILayout.VerticalScope(EditorStyles.helpBox))
				{
					GUILayout.Label("Name (" + (_mapTexture.name) + ")");
					GUILayout.Label("Resolution (x:" + _mapInstance.mapSize.x + ",y:" + _mapInstance.mapSize.y + " @" + _mapInstance.mapResolutionPower() + ")");
					GUILayout.Label("Map Size (x:" + _mapInstance.terrainMapSize.x + ",y:" + _mapInstance.terrainMapSize.y + ",z:" + _mapInstance.terrainMapSize.z);
					using (var subScope = new GUILayout.HorizontalScope())
					{
						if (GUILayout.Button("Save as: ", EditorStyles.miniButton))
						{
							SaveHeightMap();
							RefreshTextures();
						}
						_saveName = GUILayout.TextField(_saveName);
					}
				}
				GUI.backgroundColor = color5;
			}
		}
		GUI.backgroundColor = oColor;


		#region Generation

		using (var Scope = new GUILayout.VerticalScope(EditorStyles.helpBox, GUILayout.Width(EditorGUIUtility.currentViewWidth * 0.95f)))
		{
			GUI.backgroundColor = color0;
			GUILayout.Label("Change Resolution Size/Scale", EditorStyles.boldLabel);
			using (var SubScope = new GUILayout.VerticalScope(EditorStyles.helpBox))
			{
				_mapInstance.mapSize.x = EditorGUILayout.IntField("Width:", _mapInstance.mapSize.x);
				_mapInstance.mapSize.y = EditorGUILayout.IntField("Length:", _mapInstance.mapSize.y);
				_mapInstance.mapResolutionScale = EditorGUILayout.IntSlider("Scale:"+_mapInstance.mapResolutionPower().ToString(),_mapInstance.mapResolutionScale, 4, 14);
			}
			GUI.backgroundColor = oColor;

			GUI.backgroundColor = color1;
			GUILayout.Label("Change Map Size/Height", EditorStyles.boldLabel);
			using (var SubScope = new GUILayout.VerticalScope(EditorStyles.helpBox))
			{
				_mapInstance.terrainMapSize.x = EditorGUILayout.IntField("Width:", _mapInstance.terrainMapSize.x);
				_mapInstance.terrainMapSize.z = EditorGUILayout.IntField("Depth:", _mapInstance.terrainMapSize.z);
				_mapInstance.terrainMapSize.y = EditorGUILayout.IntField("Height:", _mapInstance.terrainMapSize.y);
			}
			GUI.backgroundColor = oColor;

			using (var SubScope = new GUILayout.HorizontalScope(EditorStyles.helpBox))
			{
				using (var SubSubScope = new GUILayout.VerticalScope())
				{
					GUILayout.Label("Smoothing", EditorStyles.boldLabel);
					GUI.backgroundColor = color2;
					using (var SubSubSubScope = new GUILayout.VerticalScope(EditorStyles.helpBox))
					{
						_mapInstance.genSmoothRange = EditorGUILayout.IntSlider(new GUIContent("Smooth Range", "The range of smooth iterations"), _mapInstance.genSmoothRange, 0, 4);
						_mapInstance.genSmoothLevel = EditorGUILayout.IntSlider(new GUIContent("Smooth Level", "The level of cell automata rull set"), _mapInstance.genSmoothLevel, 0, _mapInstance.smoothRange * 8);
						GUILayout.Label("For best results: \n Regenerate with Smoothing \n Range(1-2) & Level(2-6,12-15)", EditorStyles.centeredGreyMiniLabel);
					}
					GUI.backgroundColor = oColor;
				}
				using (var SubSubScope = new GUILayout.VerticalScope())
				{
					using (var SubSubSubScope = new GUILayout.HorizontalScope())
					{
						GUILayout.Label("Padding", EditorStyles.boldLabel);
						GUI.backgroundColor = color3;
						using (var SubSubySubestScope = new GUILayout.VerticalScope())
						{
							_mapInstance.activePadding = EditorGUILayout.ToggleLeft(_mapInstance.activePadding ? "as Walls" : "as Floors", _mapInstance.activePadding);
						}
						GUI.backgroundColor = oColor;
					}
					GUI.backgroundColor = color3;
					using (var SubSubSubScope = new GUILayout.VerticalScope(EditorStyles.helpBox))
					{
						_mapInstance.mapPadding.x = EditorGUILayout.IntSlider(new GUIContent("X Padding", ""), _mapInstance.mapPadding.x, 0, 50);
						_mapInstance.mapPadding.y = EditorGUILayout.IntSlider(new GUIContent("Y Padding", ""), _mapInstance.mapPadding.y, 0, 50);
						GUILayout.Label("Sets padding of heightmap \n as walls or floors, for best results \n Set Padding: (6-12)", EditorStyles.centeredGreyMiniLabel);
					}
					GUI.backgroundColor = oColor;

				}
			}
			if (GUILayout.Button("Regenerate"))
			{
				_mapInstance.Initialize();
				_mapInstance.map.SmoothAll(_mapInstance.genSmoothLevel, _mapInstance.genSmoothRange, _mapInstance.mapPadding);
				RefreshHeightMap();
				SetHeightMap();
			}
		}

		#endregion

		#region Extra Smoothing

		GUILayout.Label("Extra Smoothing", EditorStyles.boldLabel);
		using (var scope = new GUILayout.HorizontalScope(EditorStyles.helpBox, GUILayout.Width(EditorGUIUtility.currentViewWidth * 0.95f)))
		{
			using (var Scope = new GUILayout.VerticalScope(EditorStyles.helpBox))
			{
				if (GUILayout.Button("SmoothAll"))
				{
					_mapInstance.SmoothAll();
					RefreshHeightMap();

				}
				if (GUILayout.Button("SmoothFloors"))
				{
					_mapInstance.SmoothFloors();
					RefreshHeightMap();

				}
				if (GUILayout.Button("SmoothWalls"))
				{
					_mapInstance.SmoothWalls();
					RefreshHeightMap();

				}
				if (GUILayout.Button("Set to Terrain"))
				{
					SetHeightMap();
				}
			}
			GUI.backgroundColor = color4;
			using (var Scope = new GUILayout.VerticalScope(EditorStyles.helpBox))
			{
				_mapInstance.smoothRange = EditorGUILayout.IntSlider(new GUIContent("Smooth Range", "The range of smooth iterations"), _mapInstance.smoothRange, 0, 4);
				_mapInstance.smoothLevel = EditorGUILayout.IntSlider(new GUIContent("Smooth Level", "The level of cell automata rull set"), _mapInstance.smoothLevel, 0, _mapInstance.smoothRange * 8);
				GUILayout.Label("For best results: \n after Regenerating a new Height map \n" +
					"Apply extra smoothing multiple times with \n Range(1-2) & Level (3-6,12-15)", EditorStyles.centeredGreyMiniLabel);
			}
			GUI.backgroundColor = oColor;
		}
		#endregion

		#region Height Map Settings
		if (_loadContent == null)
		{
			RefreshTextures();
		}

		using (var scope = new GUILayout.HorizontalScope())
		{
			GUILayout.Label("Load Height Map", EditorStyles.boldLabel, GUILayout.Width(EditorGUIUtility.currentViewWidth * 0.425f));
			_loadSize = EditorGUILayout.Slider(_loadSize, 60, 150, GUILayout.Width(EditorGUIUtility.currentViewWidth * 0.425f));
		}
		using (var scope = new GUILayout.VerticalScope(EditorStyles.helpBox, GUILayout.Width(EditorGUIUtility.currentViewWidth * 0.95f)))
		{
			using (var subScope = new GUILayout.HorizontalScope())
			{
				if (GUILayout.Button("Refresh"))
				{
					RefreshTextures();
				}
				if (_mapTextureLoad == null && _loadContent.Count > 0)
					_mapTextureLoad = _loadContent[0];
				if (GUILayout.Button(_mapTextureLoad != null ? "Load (" + _mapTextureLoad.name + ")" : "Nothing to Load") && _mapTextureLoad != null)
				{
					_mapTexture = _mapTextureLoad;
					_saveName = _mapTexture.name;
					SetHeightMap();
				}
			}
			using (var areaScope = new GUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.MinHeight(_loadSize * 1.5f)))
			{
				_loadScroll = EditorGUILayout.BeginScrollView(_loadScroll);
				GUI.backgroundColor = color5;
				using (var subScope = new GUILayout.HorizontalScope(EditorStyles.helpBox))
				{
					foreach (Texture2D g in _loadContent)
					{
						bool sel = g == _mapTextureLoad;
						if (GUILayout.Button(new GUIContent(AssetPreview.GetAssetPreview(g), g.name + "(" + g.width + "," + g.height + ")"), sel ? EditorStyles.miniButton : EditorStyles.helpBox, GUILayout.Width(_loadSize), GUILayout.Height(_loadSize)))
						{
							_mapTextureLoad = g;
							_saveName = g.name;
						}
					}
				}
				GUI.backgroundColor = oColor;
			}
			EditorGUILayout.EndScrollView();
		}

		#endregion

		EditorGUILayout.EndScrollView();
		EditorGUIUtility.labelWidth = normalLabelWidth;

	}

	public void SaveHeightMap()
	{
		_mapTexture.filterMode = FilterMode.Bilinear;
		byte[] bytes = _mapTexture.EncodeToPNG();
		string path = pluginPath + "/Resources/" + savePath + "/" + _saveName + ".png";
		FileStream stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
		BinaryWriter writer = new BinaryWriter(stream);
		for (int i = 0; i < bytes.Length; i++)
		{
			writer.Write(bytes[i]);
		}
		writer.Close();
		stream.Close();
		AssetDatabase.ImportAsset(path);
		AssetDatabase.Refresh();
		Texture2D newTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));
		var tImporter = AssetImporter.GetAtPath(path) as TextureImporter;
		if (tImporter != null)
		{
			tImporter.textureType = TextureImporterType.Default;
			tImporter.isReadable = true;
			AssetDatabase.ImportAsset(path);
			AssetDatabase.Refresh();
		}
	}

	public void SetHeightMap()
	{
		var data = _terrainInstance.terrainData;
		var hw = data.heightmapWidth;
		var hh = data.heightmapHeight;
		var heights = data.GetHeights(0, 0, hw, hh);
		for (var y = 0; y < hh; y++)
		{
			for (var x = 0; x < hw; x++)
			{
				// normalize texture coordinates
				var x1 = 1.0f / hw * x * _mapTexture.width;
				var y1 = 1.0f / hh * y * _mapTexture.height;
				// get color height
				var pixel = _mapTexture.GetPixel((int)Mathf.FloorToInt(x1), (int)Mathf.FloorToInt(y1));
				var f = pixel.grayscale; // defines height							
				heights[x, y] = f * data.size.y;
			}
		}
		data.heightmapResolution = _mapInstance.mapResolutionPower();
		data.size = _mapInstance.terrainMapSize;
		data.SetHeights(0, 0, heights);
		_terrainInstance.terrainData = data;

	}

	public void RefreshHeightMap()
	{
		Cell[,] map = _mapInstance.map.cells;
		int width = map.GetLength(0);
		int depth = map.GetLength(1);
		_mapTexture = new Texture2D(width, depth);
		//Set pixels of texture
		for (int x = 0; x < width; x++)
		{
			for (int z = depth - 1; z >= 0; z--)
			{

				Color pixel = map[x, z].alive ? Color.white : Color.black;
				_mapTexture.SetPixel(x, z, pixel);
			}
		}
		_mapTexture.Apply();
	}

	public static bool SingletonIsValid()
	{
		if (_mapInstance == null)
		{
			UltraGenerator mapGen = FindObjectOfType<UltraGenerator>();
			if (mapGen != null)
				_mapInstance = mapGen;
			else
			{
				GameObject newGen = new GameObject("Map");
				newGen.transform.position = Vector3.zero;
				_mapInstance = newGen.AddComponent<UltraGenerator>();
			}
		}
		if (_mapTexture == null)
		{
			_mapTexture = new Texture2D(_mapInstance.terrainMapSize.x, _mapInstance.terrainMapSize.y);
		}
		if (_terrainInstance == null)
		{
			Terrain t = _mapInstance.GetComponent<Terrain>();
			if (t == null)
				t = _mapInstance.gameObject.AddComponent<Terrain>();
			TerrainData td = t.terrainData != null ? t.terrainData : new TerrainData();
			td.size = new Vector3(_mapInstance.terrainMapSize.x, _mapInstance.terrainMapSize.y, _mapInstance.terrainMapSize.z);
			t.terrainData = td;
			_terrainInstance = t;
		}
		return _mapInstance != null && _mapTexture != null;
	}

	private float GetLargestLoadSize()
	{
		float size = 100;
		foreach (Texture2D t in _loadContent)
		{
			if (t.width > t.height)
			{
				if (t.width > size)
					size = t.width;
			}
			else if (t.height > t.width)
			{
				if (t.height > size)
					size = t.height;
			}
		}
		return size;
	}

	public void RefreshTextures()
	{
		Texture2D[] texs = Resources.LoadAll<Texture2D>(savePath + "/");
		_loadContent = new List<Texture2D>();
		for (int i = 0; i < texs.Length; i++)
		{
			_loadContent.Add(texs[i]);
		}
		_loadSize = GetLargestLoadSize();
	}

	public bool SavePathIsValid()
	{
		if (!AssetDatabase.IsValidFolder(pluginPath))
		{
			AssetDatabase.CreateFolder("Assets/Plugins/", "UltraGenerator");
		}
		if (!AssetDatabase.IsValidFolder(pluginPath + "/Resources"))
		{
			AssetDatabase.CreateFolder("Assets/Plugins/UltraGenerator", "Resources");
		}
		if (!AssetDatabase.IsValidFolder(pluginPath + "/Resources/" + savePath))
		{
			AssetDatabase.CreateFolder("Assets/Plugins/UltraGenerator/Resources", savePath);
		}
		return AssetDatabase.IsValidFolder(pluginPath + "/Resources/" + savePath);
	}

}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Denchyaknow.UltraGenerator
{
	public struct Cell
	{
		public bool alive;
		
		public Cell(bool _alive)
		{
			alive = _alive;
		}
		public Cell(Cell _cell)
		{
			alive = _cell.alive;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Denchyaknow.UltraGenerator
{

	[RequireComponent(typeof(Terrain))]
	public class UltraGenerator : MonoBehaviour
	{

		[SerializeField]
		public Map map;
		public Vector2Int mapSize = new Vector2Int(100, 100);
		public Vector3Int terrainMapSize = new Vector3Int(100, 25, 100);
		public Vector2Int mapPadding = new Vector2Int(5, 5);
		public bool activePadding = false;
		public Terrain terrain;
		public int mapResolutionScale = 4;
		public int mapResolutionPower()
		{
			return (int)Mathf.Pow(2, mapResolutionScale) + 1;
		}
		public int genSmoothLevel = 5;
		public int genSmoothRange = 1;
		public int smoothLevel = 5;
		public int smoothRange = 1;
		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}
		public void Initialize()
		{
			map = new Map(mapPadding, activePadding);
			if (terrain == null)
			{
				terrain = transform.GetComponent<Terrain>();
			}
			TerrainData data = terrain.terrainData == null ? new TerrainData() : terrain.terrainData;
			data.heightmapResolution = mapResolutionPower();
			terrain.terrainData = data;
		}
		public void SmoothAll()
		{
			map.SmoothAll(smoothLevel, smoothRange, mapPadding);
		}
		public void SmoothFloors()
		{
			map.SmoothFloors(smoothLevel, smoothRange, mapPadding);
		}
		public void SmoothWalls()
		{
			map.SmoothWalls(smoothLevel, smoothRange, mapPadding);
		}
	}
}

﻿using UnityEngine;
namespace Denchyaknow.UltraGenerator
{
	public class Map
	{
		public Cell[,] cells;

		public Map()
		{
			cells = new Cell[100, 100];
			int w = cells.GetLength(0);
			int d = cells.GetLength(1);
			for (int x = 0; x < w; x++)
			{
				for (int z = 0; z < d; z++)
				{
					cells[x, z] = new Cell(Random.Range(0, 100) < 45);
				}
			}
		}
		public Map(Vector2Int padding, bool active)
		{
			cells = new Cell[100 + (padding.x * 2), 100 + (padding.y * 2)];
			int w = cells.GetLength(0);
			int d = cells.GetLength(1);
			for (int x = 0; x < w; x++)
			{
				for (int z = 0; z < d; z++)
				{

					if (x >= padding.x && z >= padding.y &&
						x >= padding.x && z <= d - padding.y &&
						x <= (w - padding.x) && z >= padding.y &&
						x <= (w - padding.x) && z <= (d - padding.y))
					{
						cells[x, z] = new Cell(Random.Range(0, 100) < 45);

					}
					else
					{
						cells[x, z] = new Cell(active);

					}
				}
			}
		}
		public Map(Cell[,] oldMap, Vector2Int padding, bool active)
		{
			cells = new Cell[100 + (padding.x * 2), 100 + (padding.y * 2)];
			int w = cells.GetLength(0);
			int d = cells.GetLength(1);
			for (int x = 0; x < w; x++)
			{
				for (int z = 0; z < d; z++)
				{

					if (x >= padding.x && z >= padding.y &&
						x >= padding.x && z <= d - padding.y &&
						x <= (w - padding.x) && z >= padding.y &&
						x <= (w - padding.x) && z <= (d - padding.y))
					{
						cells[x, z] = new Cell(Random.Range(0, 100) < 45);

					}
					else
					{
						cells[x, z] = new Cell(active);

					}
				}
			}
		}
		public Map(Cell[,] _cells)
		{
			cells = _cells;
		}

		public void SmoothAll(int level, int range, Vector2Int padding)
		{
			int w = cells.GetLength(0);
			int d = cells.GetLength(1);
			Cell[,] map = cells;
			for (int x = 0; x < w; x++)
			{
				for (int z = 0; z < d; z++)
				{
					Vector2Int pos = new Vector2Int(x, z);
					int nearbyFloors = GetNearbyFloors(cells, pos, range,padding);
					int nearbyWalls = GetNearbyWalls(cells, pos, range, padding);
					if (nearbyFloors > nearbyWalls)//Cell is to be changed to alive if is within rule of Cell Automata
					{
						if (nearbyFloors > level)
							map[x, z].alive = true;
					}
					else if (nearbyWalls > nearbyFloors)
					{
						if (nearbyWalls > level)
							map[x, z].alive = false;
					}
				}
			}
			cells = map;
		}
		public void SmoothWalls(int level, int range, Vector2Int padding)
		{
			int w = cells.GetLength(0);
			int d = cells.GetLength(1);
			Cell[,] map = cells;
			for (int x = 0; x < w; x++)
			{
				for (int z = 0; z < d; z++)
				{
					if (map[x, z].alive)
					{
						Vector2Int pos = new Vector2Int(x, z);
						int nearby = GetNearbyWalls(cells, pos, range, padding);
						if (nearby > level)//Cell is to be changed to alive if is within rule of Cell Automata
						{
							map[x, z].alive = false;
						}
					}
				}
			}
			cells = map;
		}
		public void SmoothFloors(int level, int range, Vector2Int padding)
		{
			int w = cells.GetLength(0);
			int d = cells.GetLength(1);
			Cell[,] map = cells;
			for (int x = 0; x < w; x++)
			{
				for (int z = 0; z < d; z++)
				{
					if (!map[x, z].alive)
					{
						Vector2Int pos = new Vector2Int(x, z);
						int nearby = GetNearbyFloors(cells, pos, range, padding);
						if (nearby > level)//Cell is to be changed to alive if is within rule of Cell Automata
						{
							map[x, z].alive = true;
						}
					}
				}
			}
			cells = map;
		}
		private int GetNearbyWalls(Cell[,] oldMap, Vector2Int pos, int range, Vector2Int padding)
		{
			int w = oldMap.GetLength(0);
			int d = oldMap.GetLength(1);
			int count = 0;

			for (int x = pos.x - range; x <= pos.x + range; x++)
			{
				for (int z = pos.y - range; z <= pos.y + range; z++)
				{
					if (x > 0 && x < w && z > 0 && z < d)//If cell is in bounds of old map
					{
						if (x != pos.x || d != pos.y)//If cell is not the current cell being changed
						{
							if (x >= padding.x && z >= padding.y &&
						x >= padding.x && z <= d - padding.y &&
						x <= (w - padding.x) && z >= padding.y &&
						x <= (w - padding.x) && z <= (d - padding.y))
								count += oldMap[x, z].alive ? 0 : 1;
						}
					}
				}
			}
			return count;
		}
		private int GetNearbyFloors(Cell[,] oldMap, Vector2Int pos, int range, Vector2Int padding)
		{
			int w = oldMap.GetLength(0);
			int d = oldMap.GetLength(1);
			int count = 0;
			for (int x = pos.x - range; x <= pos.x + range; x++)
			{
				for (int z = pos.y - range; z <= pos.y + range; z++)
				{
					if (x > 0 && x < w && z > 0 && z < d)//If cell is in bounds of old map
					{
						if (x != pos.x || d != pos.y)//If cell is not the current cell being changed
						{
							if (x >= padding.x && z >= padding.y &&
						x >= padding.x && z <= d - padding.y &&
						x <= (w - padding.x) && z >= padding.y &&
						x <= (w - padding.x) && z <= (d - padding.y))
								count += oldMap[x, z].alive ? 1 : 0;
						}
					}
				}
			}
			return count;
		}
	}
}

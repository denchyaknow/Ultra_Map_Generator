﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

	public Terrain mainTerrain
	{
		get
		{
			if (p_mainTerrain == null)
				p_mainTerrain = GameObject.FindGameObjectWithTag("MainTerrain").GetComponent<Terrain>();
			return p_mainTerrain;
		}
		set
		{
			p_mainTerrain = value;
		}
	}
	private Terrain p_mainTerrain;
	public Transform[,,] map;
	public MapData mapData = null;
	public Vector3Int mapBounds
	{
		get
		{
			return MapData.Bounds;
		}
	}
	public Vector3 mapSpacing = new Vector3(1, 1, 1);
	public Vector3 mapScaling = new Vector3(1, 1, 1);
	public float blockSize = 1f;
	public int mapHeight = 5;
	public float colorChange = 0.1f;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GenerateRandomCells()
	{
		mapData = new MapData(mapBounds);
	}


	private void OnDrawGizmos()
	{
		if (mapData == null)
		{
			Debug.Log("New Map");
			mapData = new MapData(mapBounds);

		}
		else
		{
			Gizmos.color = Color.red;
			float width = mapData.blocks.GetLength(0);
			//Debug.Log("Width=" + width);
			float height = mapData.blocks.GetLength(1);
			//Debug.Log("height=" + height);
			float depth = mapData.blocks.GetLength(2);
			//Debug.Log("depth=" + depth);
			Vector3 blockPos = Vector3.zero;

			//Vector3 fL = (new Vector3(0, MapData.RemapLayer * mapSpacing.y, depth * mapSpacing.z)) + (Vector3.left * (width + 1) / 4 * mapSpacing.x) + (Vector3.forward * (depth + 1) / 4 * mapSpacing.z);
			//Vector3 fR = (new Vector3(width * mapSpacing.x, MapData.RemapLayer * mapSpacing.y, depth * mapSpacing.z)) + (Vector3.right * (width + 1) / 4 * mapSpacing.x) + (Vector3.forward * (depth + 1) / 4 * mapSpacing.z);
			//Vector3 bL = (new Vector3(0, MapData.RemapLayer * mapSpacing.y, 0)) + (Vector3.left * (width + 1) / 4 * mapSpacing.x) + (Vector3.back * (depth + 1) / 4 * mapSpacing.z);
			//Vector3 bR = (new Vector3(width * mapSpacing.x, MapData.RemapLayer * mapSpacing.y, 0)) + (Vector3.right * (width + 1) / 4 * mapSpacing.x) + (Vector3.back * (depth + 1) / 4 * mapSpacing.z);

			////Gizmos.DrawLine(fL, fR);
			////Gizmos.DrawLine(fR, bR);
			////Gizmos.DrawLine(bR, bL);
			////Gizmos.DrawLine(bL, fL);
			//int borderX = (int)(width + 1) / 10;
			//int borderZ = (int)(depth + 1) / 10;
			//for (int i = -(borderX / 2); i <= borderX; i++)
			//{

			//	for (int j = -(borderZ / 2); j <= borderZ; j++)
			//	{
			//		Gizmos.DrawLine(fL + Vector3.right * i + Vector3.back * j, fR + Vector3.left * i + Vector3.back * j);
			//		Gizmos.DrawLine(fR + Vector3.left * i + Vector3.back * j, bR + Vector3.left * i + Vector3.forward * j);
			//		Gizmos.DrawLine(bR + Vector3.left * i + Vector3.forward * j, bL + Vector3.right * i + Vector3.forward * j);
			//		Gizmos.DrawLine(bL + Vector3.right * i + Vector3.forward * j, fL + Vector3.right * i + Vector3.back * j);
			//	}
			//}

			Color heightColor = new Color(1f, 1f, 0f);

			for (int x = 0; x < width; x++)
			{
				for (int z = 0; z < depth; z++)
				{
					//float floorBlock = 0f;
					float scaleX = 1;
					float scaleY = 1;
					float scaleZ = 1;
					float centerX = x;
					float centerY = 0;
					float centerZ = z;
					for (int y = 0; y < height; y++)//Set the height of the block at x,z
					{
						if (mapData.blocks[x, y, z].enabled)
						{
							scaleY = y;
						}
						else
						{
							break;
						}
					}
					scaleX = 1f * mapScaling.x * mapSpacing.x;
					scaleY *= mapScaling.y;
					scaleZ = 1f * mapScaling.z * mapSpacing.z;
					centerX *= mapSpacing.x * mapScaling.x;
					centerY = (scaleY / 2f);
					centerZ *= mapSpacing.z * mapScaling.z;
					heightColor.g = scaleY * colorChange;
					Gizmos.color = heightColor;

					Vector3 blockCenter = new Vector3(centerX, centerY, centerZ);
					Vector3 blockSize = new Vector3(scaleX, scaleY, scaleZ);
					Gizmos.DrawCube(blockCenter, blockSize);

				}
				//for (int y = 0; y < height; y++)
				//{
				//	if (mapData.blocks[x, y, z] == null)
				//	{
				//		Debug.Log("Null Block at:" + x.ToString() + "," + y.ToString() + "," + z.ToString());
				//		return;
				//	}



				//	if (mapData.blocks[x, y, z].enabled)
				//	{
				//		float percent = (y + 1) / (height);
				//		//heightColor.r = 0f + (percent * 0.5f);
				//		//heightColor.g = 1f - (percent * 0.5f);
				//		//heightColor.b = 0f + (percent * 0.5f);
				//		//heightColor.g = -(percent);
				//		//heightColor.r = (percent * colorChange);

				//		//heightColor.g = 1f - (((y + 1) / (height)) * y * colorChange);
				//		heightColor.g = y * colorChange;
				//		Gizmos.color = heightColor;

				//		blockPos = new Vector3(x * mapSpacing.x, y * mapSpacing.y, z * mapSpacing.z);
				//		Gizmos.DrawCube(blockPos, Vector3.one * blockSize);
				//	}
				//	else if(y == 0)
				//	{
				//		Gizmos.color = Color.red;
				//		blockPos = new Vector3(x * mapSpacing.x, y * mapSpacing.y, z * mapSpacing.z);
				//		Gizmos.DrawCube(blockPos, Vector3.one * blockSize);
				//	}
				//}


			}
		}
	}

}

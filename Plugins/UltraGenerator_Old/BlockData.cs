﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockData
{
	public Vector3Int position;
	public float height = 0;
	public bool enabled;
	public int state;
	public BlockData()
	{
		position = Vector3Int.zero;
		enabled = Random.Range(0, 100) < 45;
		height = Mathf.PerlinNoise(0,1);
		state = Random.Range(0, 5);
	}
	public BlockData(Vector3Int _position, bool _enabled)
	{
		position = _position;
		enabled = _enabled;
		height = 0;
		state = Random.Range(0, 4);

	}
	public BlockData(Vector3Int _position)
	{
		position = _position;
		enabled = Random.Range(0, 100) < 45;
		height = 0;
		state = Random.Range(0, 4);


	}
	public BlockData(int _state)
	{
		//enabled = _enabled;
		state = _state;

		height = _state * 0.25f;

	}
	public BlockData(bool _enabled)
	{
		enabled = _enabled;
		height = 0;
		state = Random.Range(0, 4);

	}
	public BlockData(BlockData _block)
	{
		position = _block.position;
		enabled = _block.enabled;
		height = _block.height;
		state = _block.state;

	}
}


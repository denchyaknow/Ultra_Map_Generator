﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapData
{
	//public Vector3Int bounds;
	public BlockData[,,] blocks;
	public static int SmoothRange = 1;
	public static int SmoothCount = 4;
	public static float Height = 5f;
	public static int Decay = 3;
	public static int Regeneration = 3;
	public static int Ascend = 2;
	public static int Descend = 2;
	public static int Edges = 4;
	public static int RemapLayer = 0;
	public static Vector3Int Bounds = new Vector3Int(80, 10, 80);
	public MapData()
	{
		blocks = new BlockData[Bounds.x, Bounds.y, Bounds.z];
		blocks = Fill(blocks);
	}
	public MapData(Vector3Int _bounds)
	{
		Bounds = _bounds;
		blocks = new BlockData[_bounds.x, _bounds.y, _bounds.z];
		blocks = Fill(blocks);
		//blocks = Remap(blocks);
	}
	public void SetBlock(Vector3Int _position, BlockData[,,] _block)
	{

	}
	float chanceToStartAlive = 0.45f;
	public BlockData[,,] Fill(BlockData[,,] map)
	{
		int width = map.GetLength(0);
		int height = map.GetLength(1);
		int depth = map.GetLength(2);
		for (int x = 0; x < width; x++)
		{
			for (int z = 0; z < depth; z++)
			{
				for (int y = 0; y < height; y++)
				{
					if (y == 0)
					{
						map[x, y, z] = new BlockData();
					}
					else
					{
						map[x, y, z] = new BlockData(new Vector3Int(x, y, z));
					}
					map[x, y, z].height = Random.Range(0.0f, Height);
				}
			}
		}
		return map;
	}
	//public BlockData[,,] RandomFill(BlockData[,,] map)
	//{
	//	int width = map.GetLength(0);
	//	int height = map.GetLength(1);
	//	int depth = map.GetLength(2);
	//	for (int x = 0; x < width; x++)
	//	{
	//		for (int y = 0; y < height; y++)
	//		{
	//			for (int z = 0; z < depth; z++)
	//			{
	//				map[x, y, z] = new BlockData(new Vector3Int(x, y, z));
	//			}
	//		}
	//	}
	//	return map;
	//}
	public void Remap()
	{
		int width = blocks.GetLength(0);
		int height = blocks.GetLength(1);
		int depth = blocks.GetLength(2);

		BlockData[,,] map = new BlockData[width, height, depth];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				for (int z = 0; z < depth; z++)
				{
					if (y != RemapLayer)
						blocks[x, y, z] = new BlockData(blocks[x, RemapLayer, z]);
				}
			}
		}

	}
	public static BlockData[,,] Remap(BlockData[,,] _oldMap)
	{
		int width = _oldMap.GetLength(0);
		int height = _oldMap.GetLength(1);
		int depth = _oldMap.GetLength(2);
		BlockData[,,] map = new BlockData[width, height, depth];
		for (int x = 0; x < width; x++)
		{
			for (int z = 0; z < depth; z++)
			{
				for (int y = 0; y < height; y++)
				{
					if (y == 0)
					{
						map[x, y, z] = new BlockData(_oldMap[x, 0, z]);
					}
					else
					{
						map[x, y, z] = new BlockData(_oldMap[x, y, z]);
					}
				}
			}
		}
		return map;
	}
	public static BlockData[,,] Smooth(BlockData[,,] _oldMap)
	{
		int width = _oldMap.GetLength(0);
		int height = _oldMap.GetLength(1);
		int depth = _oldMap.GetLength(2);
		BlockData[,,] map = _oldMap;
		for (int x = 0; x < width; x++)
		{
			for (int z = 0; z < depth; z++)
			{
				Vector3Int pos = new Vector3Int(x,0, z);
				int floors = GetNearbyFloors(map, pos);
				int state = 0;
				for (int s = 0; s < 5; s++)
				{
					int total = GetNearbyStates(_oldMap, pos, s);
					if (total > SmoothCount)
					{
						state = s;
					}
				}
				map[x, 0, z] = new BlockData(state);
			}
			
		}
		return map;
	}
	public static int GetNearbyStates(BlockData[,,] _oldMap, Vector3Int _pos, int _state)
	{
		int width = _oldMap.GetLength(0);
		int depth = _oldMap.GetLength(2);
		int count = 0;
		for (int x = _pos.x - SmoothRange; x <= _pos.x + SmoothRange; x++)
		{
			for (int z = _pos.z - SmoothRange; z <= _pos.z + SmoothRange; z++)
			{
				if (x > 0 && x < width && z > 0 && z < depth)
				{
					if (x != _pos.x && z != _pos.z)
					{
						if (_oldMap[x, 0, z].state == _state)
						{
							count += 1;
						}
					}
				}
			}
		}
		return count;
	}
	public static float GetHeights(BlockData[,,] _oldMap, Vector3Int _pos)
	{
		int width = _oldMap.GetLength(0);
		int depth = _oldMap.GetLength(2);
		float count = 0;
		for (int x = _pos.x - 1; x <= _pos.x + 1; x++)
		{
			for (int z = _pos.z - 1; z <= _pos.z + 1; z++)
			{
				if (x > 0 && x < width && z > 0 && z < depth)
				{
					if (x != _pos.x || z != _pos.z)
					{
						count += _oldMap[x, 0, z].height;
					}
				}
			}

		}
		return count / 8;
	}
	public static int GetNearbyFloors(BlockData[,,] _oldMap, Vector3Int _pos)
	{
		int width = _oldMap.GetLength(0);
		int depth = _oldMap.GetLength(2);
		int count = 0;
		for (int x = _pos.x - 1; x <= _pos.x + 1; x++)
		{
			for (int z = _pos.z - 1; z <= _pos.z + 1; z++)
			{
				if (x > 0 && x < width && z > 0 && z < depth)
				{
					if (x != _pos.x && z != _pos.z)
					{
						if (_oldMap[x, 0, z].state == 0)
						{
							count += 1;
						}
					}
				}
			}
		}
		return count;
	}
	public static int GetEnabled(BlockData[,,] _oldMap, Vector3Int _pos)
	{
		int width = _oldMap.GetLength(0);
		int depth = _oldMap.GetLength(2);
		int count = 0;
		for (int x = _pos.x - 1; x <= _pos.x + 1; x++)
		{
			for (int z = _pos.z - 1; z <= _pos.z + 1; z++)
			{
				if (x > 0 && x < width && z > 0 && z < depth)
				{
					if (x != _pos.x || z != _pos.z)
					{
						count += _oldMap[x, 0, z].enabled ? 1 : 0;
					}
				}
			}

		}
		return count;
	}
	public static BlockData[,,] SmoothEdges(BlockData[,,] _oldMap)
	{
		int width = _oldMap.GetLength(0);
		int height = _oldMap.GetLength(1);
		int depth = _oldMap.GetLength(2);
		BlockData[,,] map = _oldMap;
		for (int x = 0; x < width; x++)
		{
			for (int y = 1; y < height; y++)
			{
				for (int z = 0; z < depth; z++)
				{
					Vector3Int pos = new Vector3Int(x, y - 1, z);
					int floors = GetEdgeGrowth(map, pos);
					if (!_oldMap[x, y, z].enabled)
					{
						if (floors > Edges)//If enough nearby block are dead then disabled this one too
						{
							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), true);
						}
						else
						{
							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), false);

						}
					}
					else
					{
						if (floors < Edges)
						{

							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), false);
						}
						else
						{
							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), true);

						}
					}
				}
			}
		}
		return map;
	}
	public static int GetEdgeGrowth(BlockData[,,] _oldMap, Vector3Int _pos)
	{
		int width = _oldMap.GetLength(0);
		int depth = _oldMap.GetLength(2);
		int count = 0;
		for (int x = _pos.x - 1; x <= _pos.x + 1; x++)
		{
			for (int z = _pos.z - 1; z <= _pos.z + 1; z++)
			{
				if (x > 0 && x < width && z > 0 && z < depth)
				{
					if (x != _pos.x || z != _pos.z)
					{
						count += _oldMap[x, _pos.y, z].enabled ? 1 : 0;
					}
				}
			}

		}
		return count;
	}
	public static BlockData[,,] SmoothDescend(BlockData[,,] _oldMap)
	{
		int width = _oldMap.GetLength(0);
		int height = _oldMap.GetLength(1);
		int depth = _oldMap.GetLength(2);
		BlockData[,,] map = _oldMap;
		for (int x = 0; x < width; x++)
		{
			for (int y = 1; y < height; y++)
			{
				for (int z = 0; z < depth; z++)
				{
					Vector3Int pos = new Vector3Int(x, y - 1, z);
					int floors = GetDescendGrowth(map, pos);
					if (_oldMap[x, y, z].enabled)
					{
						if (floors > Descend)//If enough nearby block are dead then disabled this one too
						{
							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), false);
						}
						else
						{
							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), true);

						}
					}
					else
					{
						map[x, y, z] = new BlockData(new Vector3Int(x, y, z), false);
					}
				}
			}
		}
		return map;
	}
	public static int GetDescendGrowth(BlockData[,,] _oldMap, Vector3Int _pos)
	{
		int width = _oldMap.GetLength(0);
		int depth = _oldMap.GetLength(2);
		int count = 0;
		for (int x = _pos.x - 1; x <= _pos.x + 1; x++)
		{
			for (int z = _pos.z - 1; z <= _pos.z + 1; z++)
			{
				if (x > 0 && x < width && z > 0 && z < depth)
				{
					if (x != _pos.x || z != _pos.z)
					{
						count += _oldMap[x, _pos.y, z].enabled ? 0 : 1;
					}
				}
			}

		}
		return count;
	}
	public static BlockData[,,] SmoothAscend(BlockData[,,] _oldMap)
	{
		int width = _oldMap.GetLength(0);
		int height = _oldMap.GetLength(1);
		int depth = _oldMap.GetLength(2);
		BlockData[,,] map = _oldMap;
		for (int x = 0; x < width; x++)
		{
			for (int y = 1; y < height; y++)
			{
				for (int z = 0; z < depth; z++)
				{
					Vector3Int pos = new Vector3Int(x, y - 1, z);
					int floors = GetAscendGrowth(map, pos);
					if (!_oldMap[x, y, z].enabled)
					{
						if (floors > Ascend)//If enough nearby block are dead then disabled this one too
						{
							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), true);
						}
						else
						{
							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), false);

						}
					}
					else
					{
						map[x, y, z] = new BlockData(new Vector3Int(x, y, z), true);
					}
				}
			}
		}
		return map;
	}
	public static int GetAscendGrowth(BlockData[,,] _oldMap, Vector3Int _pos)
	{
		int width = _oldMap.GetLength(0);
		int depth = _oldMap.GetLength(2);
		int count = 0;
		for (int x = _pos.x - 1; x <= _pos.x + 1; x++)
		{
			for (int z = _pos.z - 1; z <= _pos.z + 1; z++)
			{
				if (x > 0 && x < width && z > 0 && z < depth)
				{
					if (x != _pos.x || z != _pos.z)
					{
						count += _oldMap[x, _pos.y, z].enabled ? 1 : 0;
					}
				}
			}

		}
		return count;
	}
	public static BlockData[,,] SmoothDecay(BlockData[,,] _oldMap)
	{
		int width = _oldMap.GetLength(0);
		int height = _oldMap.GetLength(1);
		int depth = _oldMap.GetLength(2);
		BlockData[,,] map = new BlockData[width, height, depth];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				for (int z = 0; z < depth; z++)
				{
					Vector3Int pos = new Vector3Int(x, y, z);
					int floors = GetFloorGrowth(_oldMap, pos);
					if (_oldMap[x, y, z].enabled)
					{
						if (floors < Decay)//If enough nearby block are dead then disabled this one too
						{
							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), false);
						}
						else
						{
							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), true);

						}
					}
					else
					{
						map[x, y, z] = new BlockData(new Vector3Int(x, y, z), false);
					}
				}
			}
		}
		return map;
	}
	public static int GetFloorGrowth(BlockData[,,] _oldMap, Vector3Int _pos)
	{
		int width = _oldMap.GetLength(0);
		int depth = _oldMap.GetLength(2);
		int count = 0;
		for (int x = _pos.x - 1; x <= _pos.x + 1; x++)
		{
			for (int z = _pos.z - 1; z <= _pos.z + 1; z++)
			{
				if (x > 0 && x < width && z > 0 && z < depth)
				{
					if (x != _pos.x || z != _pos.z)
					{
						count += _oldMap[x, _pos.y, z].enabled ? 1 : 0;
					}
				}
			}

		}
		return count;
	}
	public static BlockData[,,] SmoothRegeneration(BlockData[,,] _oldMap)
	{
		int width = _oldMap.GetLength(0);
		int height = _oldMap.GetLength(1);
		int depth = _oldMap.GetLength(2);
		BlockData[,,] map = new BlockData[width, height, depth];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				for (int z = 0; z < depth; z++)
				{
					Vector3Int pos = new Vector3Int(x, y, z);
					int walls = GetWallGrowth(_oldMap, pos);
					if (!_oldMap[x, y, z].enabled)
					{
						if (walls < Regeneration)//If enough nearby block are dead then disable this one too
						{
							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), true);
						}
						else
						{
							map[x, y, z] = new BlockData(new Vector3Int(x, y, z), false);

						}
					}
					else
					{
						map[x, y, z] = new BlockData(new Vector3Int(x, y, z), true);
					}
				}
			}
		}
		return map;
	}
	public static int GetWallGrowth(BlockData[,,] _oldMap, Vector3Int _pos)
	{
		int width = _oldMap.GetLength(0);
		int depth = _oldMap.GetLength(2);
		int count = 0;
		for (int x = _pos.x - 1; x <= _pos.x + 1; x++)
		{
			for (int z = _pos.z - 1; z <= _pos.z + 1; z++)
			{
				if (x > 0 && x < width && z > 0 && z < depth)
				{
					if (x != _pos.x || z != _pos.z)
					{
						count += _oldMap[x, _pos.y, z].enabled ? 0 : 1;
					}
				}
			}

		}
		return count;
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
[CustomEditor(typeof(MapGenerator))]
public class MapGeneratorEditor : EditorWindow
{
	private static MapGeneratorEditor _winInstance;
	private static MapGenerator _mapInstance;
	public Vector2 menuPos;
	public Vector2 menuSize;

	public Texture2D mapCurrent;
	public string mapName;
	public const string savePath = "Maps";
	public const string pluginPath = "Assets/Plugins/MapGenerator";
	public Vector2 inspectPos;
	private void OnEnable()
	{
		
	}
	private void OnDisable()
	{
		
	}

	[MenuItem("Tools/Map Generator")]
	public static void ShowWindow()
	{
		_winInstance = GetWindow<MapGeneratorEditor>(true, "Map Generator");
		if (_mapInstance == null)
		{
			MapGenerator mapGen = FindObjectOfType<MapGenerator>();
			if (mapGen != null)
				_mapInstance = mapGen;
			else
			{
				GameObject newGen = new GameObject("Map");
				newGen.transform.position = Vector3.zero;
				_mapInstance = newGen.AddComponent<MapGenerator>();
			}
		}
	}

	private void OnGUI()
	{
		if (!SavePathIsValid())
		{
			return;
		}
		Rect bottomToolbay = new Rect(menuPos.x, menuPos.y, menuSize.x, menuSize.y);

		GUIContent mapContent = new GUIContent((mapCurrent));
		EditorGUILayout.LabelField(mapContent, EditorStyles.helpBox);
		mapCurrent = (Texture2D)EditorGUILayout.ObjectField(mapCurrent, typeof(Texture2D));
		//GUILayout.Label(mapCurrent);
		if (_mapInstance == null)
		{
			MapGenerator mapGen = FindObjectOfType<MapGenerator>();
			if (mapGen != null)
				_mapInstance = mapGen;
			else
			{
				GameObject newGen = new GameObject("Map");
				newGen.transform.position = Vector3.zero;
				_mapInstance = newGen.AddComponent<MapGenerator>();
			}
		}
		if (_mapInstance.mapData == null)
		{
			_mapInstance.mapData = new MapData();
		}
		MapData.Height = EditorGUILayout.FloatField("Terrain Height", MapData.Height);
		inspectPos = EditorGUILayout.Vector2Field("Inspected Block", inspectPos);
		GUILayout.Label("Height = " + _mapInstance.mapData.blocks[(int)inspectPos.x,0,(int)inspectPos.y].height);
		GUILayout.Label("0 = " + MapData.GetNearbyStates(_mapInstance.mapData.blocks, new Vector3Int((int)inspectPos.x, 0, (int)inspectPos.y), 0).ToString());
		GUILayout.Label("1 = " + MapData.GetNearbyStates(_mapInstance.mapData.blocks, new Vector3Int((int)inspectPos.x, 0, (int)inspectPos.y),1).ToString());
		GUILayout.Label("2 = " + MapData.GetNearbyStates(_mapInstance.mapData.blocks, new Vector3Int((int)inspectPos.x, 0, (int)inspectPos.y),2).ToString());
		GUILayout.Label("3 = " + MapData.GetNearbyStates(_mapInstance.mapData.blocks, new Vector3Int((int)inspectPos.x, 0, (int)inspectPos.y),3).ToString());

		MapData.Bounds = EditorGUILayout.Vector3IntField("Map Bounds", MapData.Bounds);
		if (GUILayout.Button("Regenerate"))
		{
			//_//mapInstance.mapData = null;

			_mapInstance.GenerateRandomCells();
			mapCurrent = GetNewHeightMap();

		}
		using (var scope = new GUILayout.HorizontalScope(EditorStyles.helpBox))
		{
			if (GUILayout.Button("Apply Decay"))
			{
				_mapInstance.mapData.blocks = MapData.SmoothDecay(_mapInstance.mapData.blocks);
				mapCurrent = GetNewHeightMap();
			}
			MapData.Decay = EditorGUILayout.IntSlider("Decay Strength",MapData.Decay,0,9);

		}
		using (var scope = new GUILayout.HorizontalScope(EditorStyles.helpBox))
		{
			if (GUILayout.Button("Apply Regeneration"))
			{
				_mapInstance.mapData.blocks = MapData.SmoothRegeneration(_mapInstance.mapData.blocks);
				mapCurrent = GetNewHeightMap();

			}
			MapData.Regeneration = EditorGUILayout.IntSlider("Regeneration Strength",MapData.Regeneration, 0,9);

		}
		using (var scope = new GUILayout.HorizontalScope(EditorStyles.helpBox))
		{
			if (GUILayout.Button(_mapInstance.mapData.blocks.GetLength(1) > 1?"Remap via Layer: " + MapData.RemapLayer:"Cant Remap(Increase Height)") && _mapInstance.mapData.blocks.GetLength(1) > 1)
			{
				_mapInstance.mapData.blocks = MapData.Remap(_mapInstance.mapData.blocks);
				mapCurrent = GetNewHeightMap();

			}
			MapData.RemapLayer = EditorGUILayout.IntSlider("Layer", MapData.RemapLayer, 0, _mapInstance.mapData.blocks.GetLength(1));
		}
		using (var scope = new GUILayout.HorizontalScope(EditorStyles.helpBox))
		{
			if (GUILayout.Button(_mapInstance.mapData.blocks.GetLength(1) > 1 ? "Smooth Descend" : "Cant Descend(Increase Height)") && _mapInstance.mapData.blocks.GetLength(1) > 1)
			{
				_mapInstance.mapData.blocks = MapData.SmoothDescend(_mapInstance.mapData.blocks);
				mapCurrent = GetNewHeightMap();

			}
			MapData.Descend = EditorGUILayout.IntSlider("Descend", MapData.Descend, 0, 9);
			if (GUILayout.Button(_mapInstance.mapData.blocks.GetLength(1) > 1 ? "Smooth Ascend ": "Cant Ascend(Increase Height)") && _mapInstance.mapData.blocks.GetLength(1) > 1)
			{
				_mapInstance.mapData.blocks = MapData.SmoothAscend(_mapInstance.mapData.blocks);
				mapCurrent = GetNewHeightMap();

			}
			MapData.Ascend = EditorGUILayout.IntSlider("Ascend", MapData.Ascend, 0, 9);
		}
		using (var scope = new GUILayout.HorizontalScope(EditorStyles.helpBox))
		{
			if (GUILayout.Button( "Smooth"))
			{
				_mapInstance.mapData.blocks = MapData.Smooth(_mapInstance.mapData.blocks);
				mapCurrent = GetNewHeightMap();
			}

			MapData.SmoothRange = EditorGUILayout.IntSlider("SmoothRange", MapData.SmoothRange, 0, 10);
			MapData.SmoothCount = EditorGUILayout.IntSlider("SmoothCount", MapData.SmoothCount, 0, MapData.SmoothRange * 8);

		}
		mapName = EditorGUILayout.TextField("Name: ", mapName);
		if (GUILayout.Button("Save"))
		{
			SaveTexture(mapCurrent, pluginPath + "/" + savePath + "/" + mapName + ".png");
		}
		if (GUILayout.Button("Push to Terrain"))
		{
			TerrainData newTerrainData = _mapInstance.mainTerrain.terrainData;
			Vector3 terrainSize = newTerrainData.size;
			//float[,] heights = ConvertedBlocks(terrainSize);

			//var terrain = GetComponent<Terrain>();
			var data = _mapInstance.mainTerrain.terrainData;
			var hw = data.heightmapWidth;
			var hh = data.heightmapHeight;
			var heights = data.GetHeights(0, 0, hw, hh);
			string ranges = string.Empty;
			for (var y = 0; y < hh; y++)
			{
				for (var x = 0; x < hw; x++)
				{
					// normalize coordinates
					var x1 = 1.0f / hw * x * mapCurrent.width;
					var y1 = 1.0f / hh * y * mapCurrent.height;

					// get color height
					var pixel = mapCurrent.GetPixel((int)x1, (int)y1);
					var f = pixel.grayscale; // defines height
													 //var g = f * f * f; // some smoothing
													 //var s = 0.025f; // some scaling
					ranges += f.ToString() + ",";
					heights[x, y] = f * 50;
				}
			}

			Debug.Log(ranges);
			newTerrainData.SetHeights(0, 0, heights);
			_mapInstance.mainTerrain.terrainData = newTerrainData;

			//Vector3Int terrainLength = new Vector3Int((int)terrainSize.x,(int)terrainSize.y,(int)terrainSize.z);
			//int w = mapCurrent.width;
			//int h = mapCurrent.height;


			//float[,] scaledHeights = new float[terrainLength.x, terrainLength.z];
			//Debug.Log("Heights" + heights.GetLength(0) + "/" + heights.GetLength(1) + " ScaledHeight" + scaledHeights.GetLength(0) + "/" + scaledHeights.GetLength(1));
			//for (int x = 0; x < w; x++)
			//{
			//	for (int y = 0; y < h; y++)
			//	{
			//		heights[x, y] = mapCurrent.GetPixel(x, y).grayscale;
			//		int iX = (int)Mathf.Round((float)x * (terrainSize.x / (float)w));
			//		if (iX > scaledHeights.GetLength(0))
			//		{
			//			Debug.Log("Out of Range x:" + x.ToString() + " = iX:" + iX.ToString() + " > Length(0):" + scaledHeights.GetLength(0).ToString());
			//			return;
			//		}
			//		int iY = (int)Mathf.Round(y * (terrainSize.z / h));
			//		if (iY > scaledHeights.GetLength(1))
			//		{
			//			Debug.Log("Out of Range y:" + y.ToString() + " = iY:" + iY.ToString() + " > Length(1):" + scaledHeights.GetLength(1).ToString());
			//			return;
			//		}
			//		scaledHeights[iX,iY] = mapCurrent.GetPixel(x, y).grayscale;
			//	}
			//}
			//Vector3 terrainScale = new Vector3( MapData.Bounds.x / terrainSize.x,MapData.Bounds.y / terrainSize.y, MapData.Bounds.z / terrainSize.z);
			//Debug.Log("TerrainScale:" + terrainScale.ToString());
			//for (int x = 0; x < heights.GetLength(0); x++)
			//{
			//	for (int y = 0; y < heights.GetLength(1); y++)
			//	{
			//		int iX = (int)Mathf.Round(terrainSize.x * terrainScale.x);
			//		if (iX > scaledHeights.GetLength(0))
			//		{
			//			Debug.Log("Out of Range x:" + x.ToString() + " = iX:" + iX.ToString() + " > Length(0):" + scaledHeights.GetLength(0).ToString());
			//			return;
			//		}
			//		int iY = (int)Mathf.Round(terrainSize.y * terrainScale.z);
			//		if (iY > scaledHeights.GetLength(1))
			//		{
			//			Debug.Log("Out of Range y:" + y.ToString() + " = iY:" + iY.ToString() + " > Length(1):" + scaledHeights.GetLength(1).ToString());
			//			return;
			//		}
			//		scaledHeights[iX, iY] = heights[x,y] * terrainScale.y;

			//	}
			//}
			////newTerrainData.size = new Vector3(MapData.Bounds.x, MapData.Bounds.y, MapData.Bounds.z);
			////Bounds b = new Bounds(Vector3.zero, (Vector3)MapData.Bounds);
			////newTerrainData.size = new Vector3(w,1,h);

			//newTerrainData.SetHeights(0, 0, scaledHeights);
			//_mapInstance.mainTerrain.terrainData = newTerrainData;

			////_mapInstance.mainTerrain = _mapInstance.mainTerrain.NewTerrain();
		}
	}
	public float[,] ConvertedBlocks(Vector3 size)
	{
		BlockData[,,] blocks = _mapInstance.mapData.blocks;
		int lx = blocks.GetLength(0);
		int ly = blocks.GetLength(1);
		int lz = blocks.GetLength(2);
		float[,] heights = new float[(int)size.x, (int)size.z];
		float scaleX = (lx / size.x);
		float scaley = (ly / size.y);
		float scalez = (lz / size.z);
		for (int x = 0; x < lx; x++)
		{
			for (int z = 0; z < lz; z++)
			{
				float height = 0;
				for (int y = 0; y < ly; y++)
				{
					if (blocks[x, y, x].enabled)
					{
						height++;
					}
					else
					{
						break;
					}
				}
				int sx = (int)Mathf.Round(x * scaleX);
				float sy = (height * scaley);
				int sz = (int)Mathf.Round(z * scalez);
				heights[sx, sz] = sy;
			}
		}
		return heights;
	}
	public Texture2D GetNewHeightMap()
	{
		//Texture2D newTex = new Texture2D(MapData.Bounds.x, MapData.Bounds.z);
		Texture2D newTex = new Texture2D(MapData.Bounds.z, MapData.Bounds.z);


		//newTex.Resize(1024, 1024);
		//AssetDatabase.CreateAsset(newTex, pluginPath + "/" + savePath + "/" + newTex + ".PNG");
		newTex.Apply();
		


		BlockData[,,] map = _mapInstance.mapData.blocks;
		Color[,] pixels = new Color[MapData.Bounds.x, MapData.Bounds.z];
		int width = map.GetLength(0);
		int height = map.GetLength(1);
		int depth = map.GetLength(2);
		float grayScale = 1f / (float)height;
		Debug.Log("GrayScale is " + grayScale);
		string seed = string.Empty;
		int h = 0;

		for (int x = 0; x < width; x++)
		{
			for (int z = depth -1; z >= 0; z--)
			//int w = 0;
			{
				float he = map[x, 0, z].state / 4;
				//switch (map[x, 0, z].state)
				//{
					
				//}
				pixels[x, z] = new Color(he, he, he);
				//if (map[x, 0, z].enabled)
				//{

				//	pixels[x, z] = new Color(1, 1, 1);
				//}
				//else
				//{
				//	pixels[x, z] = new Color(0, 0, 0);

				//}
				newTex.SetPixel(x, z, pixels[x, z]);
				//for (int y = 0; y < height; y++)
				//{
				//	Color pixel = newTex.GetPixel(x, z);
				//	//grayScale = 1 * (((float)y + 1) / (float)height);
				//	if (map[x, y, z].enabled)
				//	{
				//		pixels[x, z] = new Color(y * grayScale + pixel.r,y * grayScale + pixel.g, y *grayScale + pixel.b);
				//		h++;
				//	}
				//	else
				//		pixels[x, z] = new Color( grayScale,  grayScale, grayScale);
					
				//}
				//w++;
				//seed += h.ToString();
			}
		}
		newTex.Apply();
		SetTextureImporterFormat(newTex, true);

		seed = h.ToString();
		//AssetDatabase.SaveAssets();
		return newTex;
	}
	Texture2D SaveTexture(Texture2D texture, string filePath)
	{
		texture.filterMode = FilterMode.Bilinear;

		SetTextureImporterFormat(texture, true);
		TextureScaler.Bilinear(texture, 1024, 1024);
		byte[] bytes = texture.EncodeToPNG();
		FileStream stream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
		BinaryWriter writer = new BinaryWriter(stream);
		for (int i = 0; i < bytes.Length; i++)
		{
			writer.Write(bytes[i]);
		}
		writer.Close();
		stream.Close();
		DestroyImmediate(texture);
		//I can't figure out how to import the newly created .png file as a texture
		Debug.Log("Made PNG at " + filePath);
		
		AssetDatabase.ImportAsset(filePath);
		
		AssetDatabase.Refresh();
		Texture2D newTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(filePath, typeof(Texture2D));
		
		if (newTexture == null)
		{
			Debug.Log("Couldn't Import");
		}
		else
		{
			Debug.Log("Import Successful");
		}
		newTexture.filterMode = FilterMode.Bilinear;
		SetTextureImporterFormat(newTexture, true);

		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
		return newTexture;
	}
	public static void SetTextureImporterFormat(Texture2D texture, bool isReadable)
	{
		if (null == texture)
			return;

		string assetPath = AssetDatabase.GetAssetPath(texture);
		var tImporter = AssetImporter.GetAtPath(assetPath) as TextureImporter;
		if (tImporter != null)
		{
			tImporter.textureType = TextureImporterType.Default;

			tImporter.isReadable = isReadable;

			AssetDatabase.ImportAsset(assetPath);
			AssetDatabase.Refresh();
		}
	}
	public bool SavePathIsValid()
	{
		if (!AssetDatabase.IsValidFolder(pluginPath + "/" + savePath))
		{
			AssetDatabase.CreateFolder("Assets/Plugins/MapGenerator",savePath);
		}
		return AssetDatabase.IsValidFolder(pluginPath + "/" + savePath);
	}

}
public static class exts
{
	public static Terrain NewTerrain(this Terrain ot)
	{
		Terrain t = ot.gameObject.AddComponent<Terrain>();
		t.terrainData = new TerrainData();
		ot.gameObject.name = Random.Range(0, 100).ToString();

		return t;
	}
	public static TerrainData Instance(this TerrainData td)
	{
		TerrainData nt = new TerrainData();


		return nt;
	}
}
